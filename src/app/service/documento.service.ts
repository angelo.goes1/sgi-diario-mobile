import { Documento } from './../models/documento.model';
import { buscaRequestPayload } from './../models/requests/buscaRequestPayload.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { pdfRequestPayload } from '../models/requests/pdfRequestPayload.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class DocumentoService {
  docs?: Documento[];
  pdfSrc: string = '';
  documentoInfoApi: string =
    'http://sgidom.parnamirim.rn.gov.br/rest/sgidiario_portaria_service/busca_geral';

  constructor(private http: HttpClient, private router: Router) {}

  fetchDocumentDataByBusca(
    busca: buscaRequestPayload
  ): Observable<Documento[]> {
    var docsApi =
      'https://api.allorigins.win/get?url=' +
      encodeURIComponent(
        this.documentoInfoApi +
          '?termogeral=' +
          busca.termogeral +
          '&idTipoDoc=' +
          (busca.idTipoDoc || '') +
          '&dataInicial=' +
          (busca.dataInicial || '') +
          '&dataFinal=' +
          (busca.dataFinal || '') +
          '&idSec=' +
          (busca.idSec || '') +
          '&pagina=' +
          busca.pagina +
          '&dom=' +
          (busca.dom || '')
      );

    return this.http
      .get<any>(`${docsApi}`)
      .pipe(map((x) => JSON.parse(x.contents)));
  }

  // fetchPDFByDocumentData(documento: Documento): void {
  //   this.http
  //     .post(
  //       `${environment.api}`,
  //       new pdfRequestPayload(
  //         documento.data_publicacao,
  //         environment.domOrigin,
  //         undefined,
  //         documento.id
  //       ),
  //       {
  //         responseType: 'blob',
  //       }
  //     )
  //     .subscribe((resultBlob: Blob) => {
  //       const docBlob = new Blob([resultBlob], { type: 'application/pdf' });
  //       const docURL = URL.createObjectURL(docBlob);
  //       this.pdfSrc = docURL;
  //       this.router.navigate(['/view']);
  //     });
  // }

  // getPDFSource(): string {
  //   if (!this.pdfSrc) this.router.navigate(['/home']);
  //   return this.pdfSrc;
  // }
}
