import { Diario } from './../models/diario.model';
import { Injectable } from '@angular/core';
import { Documento } from '../models/documento.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { pdfRequestPayload } from '../models/requests/pdfRequestPayload.model';

@Injectable({
  providedIn: 'root',
})
export class PdfService {
  pdfSrc: string = '';

  constructor(private http: HttpClient, private router: Router) {}

  fetchPDF(doc: Documento | Diario): void {
    this.http
      .post(
        `${environment.api}`,
        new pdfRequestPayload(
          doc.data_publicacao,
          environment.domOrigin,
          (doc.diario ? undefined : doc.id),
          (doc.diario ? doc.id : undefined)
        ),
        {
          responseType: 'blob',
        }
      )
      .subscribe((resultBlob: Blob) => {
        const docBlob = new Blob([resultBlob], { type: 'application/pdf' });
        const docURL = URL.createObjectURL(docBlob);
        this.pdfSrc = docURL;
        this.router.navigate(['/view']);
      });
  }

  getPDFSource(): string {
    if (!this.pdfSrc) this.router.navigate(['/home']);
    return this.pdfSrc;
  }
}
