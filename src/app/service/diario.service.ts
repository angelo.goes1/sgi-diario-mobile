import { PdfService } from './pdf.service';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as dayjs from 'dayjs';
import { map, Observable } from 'rxjs';

import { Diario } from '../models/diario.model';
import { pdfRequestPayload } from '../models/requests/pdfRequestPayload.model';

@Injectable({
  providedIn: 'root',
})
export class DiarioService {
  pdfSrc: string = '';
  diarioInfoApi: string =
    'http://sgidom.parnamirim.rn.gov.br/rest/sgidiario_diario_service/diarios_por_mes';

  constructor(private http: HttpClient, private router: Router, private pdfService: PdfService) {}

  buildPDFSourceByDate(date: Date) {
    this.fetchDiaryDataByMonthAndYear(date).subscribe((diarios) => {

      var unix = dayjs(dayjs(date).format('YYYY-MM-DD')).valueOf();
      var filteredDiarios = diarios.filter(function (i: any) {
        return i.data_publicacao === unix;
      });
      
      if (filteredDiarios.length > 0) {
        if (filteredDiarios.length > 2) {
          console.log('Mais de um diario no dia!');
          return;
        }

        console.log(
          'Carregando e redirecionando para diario do dia ' +
            dayjs(date).format('DD/MM/YYYY') +
            ' (id=' +
            filteredDiarios[0].id +
            ')'
        );

        this.pdfService.fetchPDF(filteredDiarios[0]);
      } else {
        console.log('Nenhum diario publicado hoje!');
      }
    });
  }

  fetchDiaryDataByMonthAndYear(date: Date): Observable<Diario[]> {
    var docsApi =
      'https://api.allorigins.win/get?url=' +
      encodeURIComponent(
        this.diarioInfoApi + '?data=' + dayjs(date).format('YYYY-MM')
      );

    return this.http
      .get<any>(`${docsApi}`)
      .pipe(map((x) => JSON.parse(x.contents)));
  }

  // fetchPDFByDiaryData(diario: Diario): void {
  //   this.http
  //     .post(
  //       `${environment.api}`,
  //       new pdfRequestPayload(
  //         diario.data_publicacao,
  //         environment.domOrigin,
  //         diario.id
  //       ),
  //       {
  //         responseType: 'blob',
  //       }
  //     )
  //     .subscribe((resultBlob: Blob) => {
  //       const diaryBlob = new Blob([resultBlob], { type: 'application/pdf' });
  //       const diaryURL = URL.createObjectURL(diaryBlob);
  //       this.pdfSrc = diaryURL;
  //       this.router.navigate(['/view']);
  //     });
  // }

  // getPDFSource(): string {
  //   if (!this.pdfSrc) this.router.navigate(['/home']);
  //   return this.pdfSrc;
  // }
}
