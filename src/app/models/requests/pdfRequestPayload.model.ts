import * as dayjs from 'dayjs';

export class pdfRequestPayload {
  diarioId?: number | string;
  domDataCabecalho: string;
  domOrigin: string;
  domQueryParams: string;

  constructor(
    unixDate: number,
    domOrigin: string,
    diarioId?: number,
    docId?: number
  ) {
    this.diarioId = diarioId ? diarioId : '';
    this.domDataCabecalho = dayjs(unixDate).format('DD/MM/YYYY');
    this.domOrigin = domOrigin;
    this.domQueryParams =
      'publicar=false&' +
      (diarioId ? `id_diario=${diarioId}` : `id_documento=${docId}`);
  }
}
