export interface buscaRequestPayload {
  termogeral: string;
  idTipoDoc?: number;
  dataInicial?: string;
  dataFinal?: string;
  idSec?: number;
  pagina: number;
  dom?: string;
}
