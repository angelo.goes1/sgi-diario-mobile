export interface Diario {
    id: number;
    numero: string;
    data_publicacao: number;

    // Existe somente para ser 'null' e diferenciar de documento.model...
    // ... dentro de pdf.service.ts 
    diario: Diario;
}