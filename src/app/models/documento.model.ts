import { docType } from './doctype.model';
import { Diario } from './diario.model';
export interface Documento {
    id: number;
    numero: string;
    data_publicacao: number;
    descricao: string;
    diario: Diario;
    tipo_documento: docType;
    abreviatura: string;
}