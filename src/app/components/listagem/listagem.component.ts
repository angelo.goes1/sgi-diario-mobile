import { DocumentoService } from './../../service/documento.service';
import { Documento } from './../../models/documento.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.scss'],
})
export class ListagemComponent implements OnInit {
  loadedDocs: boolean = false;
  docs: Documento[] = [];

  constructor(private documentoService: DocumentoService) {
    this.documentoService
      .fetchDocumentDataByBusca({
        termogeral: 'B',
        idTipoDoc: 2,
        pagina: 2,
      })
      .subscribe((ref) => {
        this.docs = ref;
        console.log(this.docs);
        this.loadedDocs = true;
      });
  }

  ngOnInit(): void {}
}
