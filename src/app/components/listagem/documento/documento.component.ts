import { DiarioService } from './../../../service/diario.service';
import { Diario } from './../../../models/diario.model';
import { PdfService } from './../../../service/pdf.service';
import { Documento } from './../../../models/documento.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-documento',
  templateUrl: './documento.component.html',
  styleUrls: ['./documento.component.scss'],
})
export class DocumentoComponent implements OnInit {
  @Input() dados!: Documento;

  constructor(
    private pdfService: PdfService,
    private diarioService: DiarioService
  ) {}

  ngOnInit(): void {}

  openDocument() {
    this.pdfService.fetchPDF(this.dados);
  }

  openDiary() {
    this.diarioService.buildPDFSourceByDate(
      new Date(this.dados.data_publicacao)
    );
  }
}
