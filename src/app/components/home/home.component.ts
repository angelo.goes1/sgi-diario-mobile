import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DiarioService } from '../../service/diario.service';
import { Documento } from './../../models/documento.model';
import { DocumentoService } from './../../service/documento.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  dateUrl: string =
    'http://sgidom.parnamirim.rn.gov.br/rest/sgidiario_utils/data';
  dateApi: string =
    'https://api.allorigins.win/get?url=' + encodeURIComponent(this.dateUrl);

  currTime!: Date;
  isLoaded: boolean = false;
  docs?: Documento[];

  constructor(
    private http: HttpClient,
    private diarioService: DiarioService,
    private router: Router
  ) {
    this.http.get<any>(this.dateApi).subscribe((date) => {
      var unixTime = Date.parse(date.contents);
      this.currTime = new Date(unixTime);
      this.isLoaded = true;
    });
  }

  ngOnInit(): void {}

  goToTodaysDiary() {
    this.diarioService.buildPDFSourceByDate(this.currTime);
  }

  search() {
    this.router.navigate(['/listagem']);
  }
}
