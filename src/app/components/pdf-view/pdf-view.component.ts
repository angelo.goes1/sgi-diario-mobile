import { PdfService } from './../../service/pdf.service';
import { DocumentoService } from 'src/app/service/documento.service';
import 'hammerjs';

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PDFDocumentProxy } from 'ng2-pdf-viewer';

import { DiarioService } from '../../service/diario.service';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-pdf-view',
  templateUrl: './pdf-view.component.html',
  styleUrls: ['./pdf-view.component.scss'],
})
export class PdfViewComponent implements OnInit {
  // PDF variables
  src: string;
  page: number = 1;
  lastPage: number = 0;
  totalPages: number = 0;
  isLoaded: boolean = false;

  // Timer variables
  navVisible: boolean = true;
  slider = document.getElementById('slider');
  timeLeft: number = environment.sliderFadeDelay;
  interval: any;

  constructor(private pdfService: PdfService) {
    this.src = pdfService.getPDFSource();
  }

  ngOnInit(): void {
    this.startTimer();
  }

  afterLoadComplete(pdfData: PDFDocumentProxy) {
    this.totalPages = pdfData.numPages;
    this.isLoaded = true;
  }

  nextPage() {
    if (this.page < this.totalPages) this.page++;
    this.resetTimer();
  }

  prevPage() {
    if (this.page > 1) this.page--;
    this.resetTimer();
  }

  changePage(event: any) {
    this.page = event.target.value;
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.navVisible = false;
      }
    }, 1000);
  }

  resetTimer() {
    clearInterval(this.interval);
    this.timeLeft = environment.sliderFadeDelay;
    this.navVisible = true;
    this.startTimer();
  }
}
